def COLOR_MAP = [
    'SUCCESS': 'good',
    'FAILURE': 'danger',
    'ABORTED': 'warning',
    'UNSTABLE': 'warning',
]

pipeline {
  agent any

tools {
        maven 'maven3'
        jdk 'OracleJDK8'
    }
    environment {
        PATH_TO_HOST_FOLDER = pwd()
        SNAP_REPO = 'eawangya-snapshot'
        NEXUS_USER = 'admin'
        NEXUS_PASS = 'admin123'
        RELEASE_REPO = 'eawangya-release'
        CENTRAL_REPO = 'eawangya-maven-central'
        NEXUSIP = '192.168.56.18'
        NEXUSPORT = '8081'
        NEXUS_GRP_REPO = 'eawangya-group'
        NEXUS_LOGIN = 'nexus-creds'
        SONARSERVER = 'sonarserver'
        SONARSCANNER = 'sonarscanner'
        AWS_ACCESS_KEY_ID     = credentials('aws_access_key_id')
        AWS_SECRET_ACCESS_KEY = credentials('aws_secret_access_key')
        ISTIO_VERSION = '1.20.0'
        ISTIO_INSTALL_DIR = "/var/lib/jenkins/workspace/owasp/istio-${ISTIO_VERSION}"
        ISTIO_PATH = "/var/lib/jenkins/workspace/owasp/istio-1.20.0/bin"
      
    }    

  stages {
    // stage('Clean WorkSpace') {
    //     steps {
    //         cleanWs()
    //     }
    // }
        stage('GitLeaks Scan') {
            steps {
                catchError(buildResult: 'SUCCESS') {
                    script {
                        sh 'docker pull zricethezav/gitleaks:latest'
                        sh "docker run --rm -u 0 -v \"${PATH_TO_HOST_FOLDER}\":/path zricethezav/gitleaks:latest detect --source=/path --report-format json --report-path /path/gitleaks_scan.json"
                        archiveArtifacts artifacts: 'gitleaks_scan.json', fingerprint: true
                    }
                }
            }
        }    
    stage('Test') {
      steps {

          sh "mvn test"

      }
    }
    stage('Build') {
      steps {
        script {
          sh 'mvn package'
        }
      }
    }
    stage('CheckStyle Analysis') {
      steps {
        script {
          sh 'mvn checkstyle:checkstyle'
        }
      }
    }
    stage('OWASP Dependency-Check Vulnerabilities') {
        steps {
            script {
                catchError(buildResult: 'SUCCESS', allowEmpty: true) {
                    dependencyCheck additionalArguments: '', odcInstallation: 'DP-check'
                    dependencyCheckPublisher pattern: '**/dependency-check-report.xml'
                  }
              }
          }
      }
       stage('SonarQube Analysis') {
          environment {
              def scannerHome = tool "${SONARSCANNER}"
          }
          steps {
              withSonarQubeEnv("${SONARSERVER}") {
                sh 'mvn sonar:sonar -Dsonar.dependencyCheck.jsonReportPath=target/dependency-check-report.json -Dsonar.dependencyCheck.xmlReportPath=target/dependency-check-report.xml -Dsonar.dependencyCheck.htmlReportPath=target/dependency-check-report.html'
        } 
      }
    }
    stage("Quality Gate"){
        steps {
            timeout(time: 60, unit: 'MINUTES') {
            waitForQualityGate abortPipeline: true
        }
      }
    }
        stage("Upload Artifact to Nexus") {
            steps {
                nexusArtifactUploader(
                    nexusVersion: 'nexus3',
                    protocol: 'http',
                    nexusUrl: "${NEXUSIP}:${NEXUSPORT}",
                    groupId: 'QA',
                    version: "${env.BUILD_ID}-${env.BUILD_TIMESTAMP}",
                    repository: "${RELEASE_REPO}",
                    credentialsId: "${NEXUS_LOGIN}",
                    artifacts: [
                        [artifactId: 'hexaware',
                            classifier: '',
                            file: './target/hexaware-v2.war',
                            type: 'war']
                    ]
                )
            }
        }
        stage('Build Docker') {
            steps {
                script {
                    sh 'docker build -t eawangya/techharbor:version2 .'
                }
            }
        }

        stage('Trivy Image Scan') {
            steps {
                sh 'trivy image eawangya/techharbor:version2'
            }
        }
stage('Docker Push') {
    steps {
        script {
            withCredentials([usernamePassword(credentialsId: 'dockerhub-creds', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                sh "echo \${PASS} | docker login -u \${USER} --password-stdin" 
                sh "docker push eawangya/techharbor:version2" 

                // Clean Jenkins Server
                sh "docker rmi -f \$(docker images -qa)"
            }
        }
    }
} 
 
        stage('Provision AKS cluster') {
            steps {
                script {
                    dir('terraform-eks-cluster') {
                        sh 'terraform init'
                        sh 'terraform apply --auto-approve'
                    }
                }
            }
        }
        // Istio, a powerful service mesh, is seamlessly installed and configured to enhance service communication and observability.
        stage('Install and Configure Istio') {
            steps {
                script {
                    dir('kubenetes-files') {
                        sh "aws eks --region us-east-1 update-kubeconfig --name hexaware-eks--cluster"
                    }
                    sh "curl -L https://istio.io/downloadIstio | ISTIO_VERSION=${ISTIO_VERSION} sh -"

                    dir("istio-1.20.0") {
                        sh 'export PATH="$PATH:/var/lib/jenkins/workspace/owasp/istio-1.20.0/bin"'
                        sh '${ISTIO_PATH}/istioctl install --set profile=demo -y'
                        sh 'kubectl label namespace default istio-injection=enabled'
                    }
                }
            }
        }       
        stage('Deploy App on EKS') {
            steps {
                script {
                    dir('kubenetes-files') {
                        sh 'kubectl apply -f .'
                        // Add wait statements if needed
                    }
                }
            }
        }
        // Istio-specific analysis is performed to ensure optimal service mesh functionality and performance.
        stage('Istio Analyze') {
            steps {
                script {
                    dir("istio-1.20.0") {
                        // sh '${ISTIO_PATH}/istioctl analyze'
                        // sh 'kubectl apply -f samples/addons'
                        // sh 'kubectl rollout status deployment/kiali -n istio-system'

                        // CLEAN UP
                        sh 'kubectl delete -f samples/addons'
                        sh '${ISTIO_PATH}/istioctl uninstall -y --purge'
                        sh 'kubectl delete namespace istio-system'
                        sh 'kubectl label namespace default istio-injection-'
                    }
                }
            }
        }
   }
    // // Finally, upon successful deployment and analysis, automated notifications are sent to Slack, keeping the team informed in real-time.
    post {
        always {
            echo 'Slack Notification.'
            slackSend channel: '#devops-cicd-pipeline',
                color: COLOR_MAP[currentBuild.currentResult],
                message: "*${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} \n More info at: ${env.BUILD_URL}"
        }
    }
}
