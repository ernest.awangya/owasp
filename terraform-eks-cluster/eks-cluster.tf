#Provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.31.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"

  default_tags {
    tags = {
      Name = "create-by-terraform-hexaware-eks"
    }
  }
}


#Resources
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc
resource "aws_vpc" "hexaware-eks--vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "hexaware-eks-cluster"
  }
}
# VPC-2
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet
resource "aws_subnet" "hexaware-eks--public" {
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-1a"
  vpc_id                  = aws_vpc.hexaware-eks--vpc.id
  map_public_ip_on_launch = true
  tags = {
    Name = "hexaware-eks--public"
  }
}



# VPC-03
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet
resource "aws_subnet" "hexaware-eks--public-2" {
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "us-east-1b"
  vpc_id                  = aws_vpc.hexaware-eks--vpc.id
  map_public_ip_on_launch = true
  tags = {
    Name = "hexaware-eks--public-2"
  }
}

# VPC-05
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway
resource "aws_internet_gateway" "hexaware-eks--internet-gateway" {
  vpc_id = aws_vpc.hexaware-eks--vpc.id
  tags = {
    Name = "hexaware-eks--internet-gateway"
  }
}


# VPC-04
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route

resource "aws_route" "eks-internet_access" {
  route_table_id         = aws_vpc.hexaware-eks--vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.hexaware-eks--internet-gateway.id

}

# eks-cluster
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster
resource "aws_eks_cluster" "hexaware-eks--cluster" {
  name     = "hexaware-eks--cluster"
  version  = "1.28"
  role_arn = aws_iam_role.hexaware-eks--cluster-admin-role.arn

  vpc_config {
    subnet_ids              = [aws_subnet.hexaware-eks--public.id, aws_subnet.hexaware-eks--public-2.id]
    endpoint_public_access  = true
    endpoint_private_access = true
    public_access_cidrs     = ["0.0.0.0/0"]
  }
  depends_on = [
    aws_iam_role_policy_attachment.hexaware-eks--cluster-AmazonEKSClusterPolicy, aws_iam_role_policy_attachment.hexaware-eks--cluster-AmazonEKSVPCResourceController
  ]
  tags = {
    demo = "eks"
  }
}

# eks-cluster-2
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document
data "aws_iam_policy_document" "hexaware-eks--cluster-admin-role-policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

# eks-cluster-03
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role
resource "aws_iam_role" "hexaware-eks--cluster-admin-role" {
  name               = "hexaware-eks--cluster-admin-role"
  assume_role_policy = data.aws_iam_policy_document.hexaware-eks--cluster-admin-role-policy.json
}

# eks-cluster-04
resource "aws_iam_role_policy_attachment" "hexaware-eks--cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.hexaware-eks--cluster-admin-role.name
}

# eks-cluster-05
# Optionally, enable Security Groups for Pods
# Reference: https://docs.aws.amazon.com/eks/latest/userguide/security-groups-for-pods.html
resource "aws_iam_role_policy_attachment" "hexaware-eks--cluster-AmazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.hexaware-eks--cluster-admin-role.name
}

# eks-cluster-06
# Addones
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_addon

resource "aws_eks_addon" "hexaware-eks--addon-coredns" {
  cluster_name                = aws_eks_cluster.hexaware-eks--cluster.name
  addon_name                  = "coredns"
  addon_version               = "v1.10.1-eksbuild.2"
  resolve_conflicts_on_create = "OVERWRITE"
}

# eks-cluster-07
resource "aws_eks_addon" "hexaware-eks--addon-kube-proxy" {
  cluster_name                = aws_eks_cluster.hexaware-eks--cluster.name
  addon_name                  = "kube-proxy"
  addon_version               = "v1.28.1-eksbuild.1"
  resolve_conflicts_on_create = "OVERWRITE"
}

# eks-cluster-08
resource "aws_eks_addon" "hexaware-eks--addon-vpc-cni" {
  cluster_name                = aws_eks_cluster.hexaware-eks--cluster.name
  addon_name                  = "vpc-cni"
  addon_version               = "v1.14.1-eksbuild.1"
  resolve_conflicts_on_create = "OVERWRITE"
}


output "endpoint" {
  value = aws_eks_cluster.hexaware-eks--cluster.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.hexaware-eks--cluster.certificate_authority[0].data
}


# eks-cluster-09
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_node_group

resource "aws_eks_node_group" "hexaware-eks--ec2-node-group" {
  cluster_name    = aws_eks_cluster.hexaware-eks--cluster.name
  node_group_name = "hexaware-eks--node-group"
  node_role_arn   = aws_iam_role.hexaware-eks--ec2-node-group-role.arn
  subnet_ids      = [aws_subnet.hexaware-eks--public.id, aws_subnet.hexaware-eks--public-2.id]
  instance_types  = ["t3.medium"]

  scaling_config {
    desired_size = 1
    max_size     = 1
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.hexaware-eks--node-group-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.hexaware-eks--node-group-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.hexaware-eks--node-group-AmazonEC2ContainerRegistryReadOnly,
  ]
  labels = {
    demo         = "eks",
    eksNodeGroup = "t3_medium"
  }
}

# eks-cluster-10
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role
resource "aws_iam_role" "hexaware-eks--ec2-node-group-role" {
  name = "hexaware-eks--ec2-node-group-role"
  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

# eks-cluster-11
resource "aws_iam_role_policy_attachment" "hexaware-eks--node-group-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.hexaware-eks--ec2-node-group-role.name
}

# eks-cluster-12
resource "aws_iam_role_policy_attachment" "hexaware-eks--node-group-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.hexaware-eks--ec2-node-group-role.name
}

# eks-cluster-13
resource "aws_iam_role_policy_attachment" "hexaware-eks--node-group-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.hexaware-eks--ec2-node-group-role.name
}
